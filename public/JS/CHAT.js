import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import { getDatabase, ref, set, push, onValue, orderByKey, query, limitToLast, child} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";
import {getAuth, onAuthStateChanged, signOut} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js'


const firebaseConfig = {
    apiKey: "AIzaSyCz97bH-OGtG5mNlvO1uWdcFFhULs5DnsA",
    authDomain: "tp-integrador-luca-damiano.firebaseapp.com",
    databaseURL: "https://tp-integrador-luca-damiano-default-rtdb.firebaseio.com",
    projectId: "tp-integrador-luca-damiano",
    storageBucket: "tp-integrador-luca-damiano.appspot.com",
    messagingSenderId: "18187202680",
    appId: "1:18187202680:web:8b2a26a34453845e023d8d"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth(app);
const user = auth.currentUser;
var UID;
var correo;
var name1;
var city;

console.log("Consola de pruebas...");

// Referencias al HTML
let mensajeRef = document.getElementById("textoChatId");
let botonEnviarRef = document.getElementById("buttonChatId");
let chatRef = document.getElementById("mensajeChatId");
let botonLogOutRef = document.getElementById("buttonLogOut");

let nombreyapellidoRef = document.getElementById("nombreyapellidoRegisterId");
let ciudadRef = document.getElementById("ciudadRegisterId");


// Event Listeners
botonEnviarRef.addEventListener("click", cargarMensaje);
botonLogOutRef.addEventListener("click", logOut);

// Funciones

onAuthStateChanged(auth, (user) => {
    if (user) {
      
        const uid = user.uid;
        UID = uid;
        
             
        
        const email = user.email;
        correo = email;

        console.log("El usuario activo es: " + correo + " UID: " + UID)


        botonEnviarRef.addEventListener("click", cargarMensaje);

    } else {        
    }
});

function userNoRegistrado(){
    window.location.href = "../pages/LOGIN_SIGNIN.html"
    alert ("Inicia sesión para enviar un mensaje");
}  

function cargarMensaje(){

    let mensaje = mensajeRef.value;
    
    push(ref(database, "mensaje/"), {
        msg: mensaje,
        email: correo, 
    });

    mensajeRef.value = "";
    console.log("Mensaje enviado.");
    location.reload()
}

const queryMensajes = query(ref(database, "mensaje/"), orderByKey('msg'));

onValue(queryMensajes, (snapshot) => {
    
    snapshot.forEach((childSnapshot) =>{
        const childKey = childSnapshot.key;
        const message = childSnapshot.val();

        chatRef.innerHTML += `
        <p>${message.email}: ${message.msg}</p>`
    })
})

function logOut(){

    const auth = getAuth();
    signOut(auth).then(() => {
    window.location.href ="../CHAT.html";
    })
    
    .catch((error) => {
    });
}  



