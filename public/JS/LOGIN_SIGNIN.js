import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set, push, onValue } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";


const firebaseConfig = {
    apiKey: "AIzaSyCz97bH-OGtG5mNlvO1uWdcFFhULs5DnsA",
    authDomain: "tp-integrador-luca-damiano.firebaseapp.com",
    databaseURL: "https://tp-integrador-luca-damiano-default-rtdb.firebaseio.com",
    projectId: "tp-integrador-luca-damiano",
    storageBucket: "tp-integrador-luca-damiano.appspot.com",
    messagingSenderId: "18187202680",
    appId: "1:18187202680:web:8b2a26a34453845e023d8d"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);

console.log("Consola de pruebas...");

// Referencias al HTML
let correoLogInRef = document.getElementById("correoLogInId");
let passLogInRef = document.getElementById("passwordLogInId");

let correoRegisterRef = document.getElementById("correoRegisterId");
let passRegisterRef = document.getElementById("passwordRegisterId");

let nombreyapellidoRef = document.getElementById("nombreyapellidoRegisterId");
let ciudadRef = document.getElementById("ciudadRegisterId");

let buttonRegisterRef = document.getElementById("RegisterButtonId");
let buttonLogInRef = document.getElementById("LogInButtonId");

// Event Listeners
buttonRegisterRef.addEventListener("click", registerUser);

buttonLogInRef.addEventListener("click", loginUser);

// Funciones
const auth = getAuth();

function registerUser (){

    console.log("Ingreso a la función registerUser().");

    if((correoRegisterRef.value != '') && (passRegisterRef.value != '') && (nombreyapellidoRef.value != '') && (ciudadRef.value != '')){

        createUserWithEmailAndPassword(auth, correoRegisterRef.value, passRegisterRef.value)

        .then((userCredential) => {

            const user = userCredential.user;
            const uid = user.uid;

            console.log("Usuario: " + nombreyapellidoRef.value + " ID: " + uid);
            console.log("Creación de usuario.");

            set(ref(database, "datosdelusuario/" + nombreyapellidoRef.value), {
                Nombre: nombreyapellidoRef.value, 
                Ciudad: ciudadRef.value,
                Mail: correoRegisterRef.value,
                UID: uid,

            });
        })

        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
         alert("Revisar que los campos de usuario y contraseña esten completos.");
    }

}


function loginUser(){

    console.log("Ingreso a la función altaUsuario().");

    if((correoLogInRef.value != '') && (passLogInRef.value != '')){

        signInWithEmailAndPassword(auth, correoLogInRef.value, passLogInRef.value)

        .then((userCredential) => {
            console.log("Usuario logeado correctamente.");

            window.location.href = "../pages/LOGUEADO.html";
        })

        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos esten completos.");
    }
}